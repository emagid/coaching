<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: About Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>


	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1>GET TO KNOW US</h1>
<!--                <a href='/contact' class='button'> JOIN THE CLUB </a>-->
            </div>
        </div>
	</section>
    <section class="small_hero">
            <div class='text_box'>
                <p>Become the professional you’ve always wanted to be.</p>
                <a href=""><h4>Register</h4></a>
            </div>
    </section>

	<!-- HERO SECTION END -->


    <section class='bio'>
        <h2 class="auto_center"><?php the_field('title'); ?></h2>
        <?php the_field('content'); ?>
            <div class="talk_button">
                <div>
                    <a href="/contact">
                       <button class='lets_talk button'> LETS TALK </button>
                    </a>
                </div>
            </div>
	</section>

<!--
    <section class='grey_tone'>
        <h2>Our Investments / Partners</h2>
        
        <div class="company_logo"> 
                        <//?php
	  			$args = array(
	    		'post_type' => 'clients'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
            <a href="<?php the_permalink(); ?>">
                <img src="<?php the_field('logo'); ?>">
            </a>
                   <//?php
			}
				}
			else {
			echo 'No Clients Found';
			}
		?>   
        </div>
	</section>
-->



<?php
get_footer();
