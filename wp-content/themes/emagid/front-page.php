<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: Front Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>



	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1><?php the_field('hero_title'); ?></h1>
                <p><?php the_field('hero_subtitle'); ?></p>
                <a href='/contact' class='button'> GET STARTED </a>
            </div>
        </div>
	</section>
    <section class="small_hero">
            <div class='text_box'>
                <p>Become the professional you’ve always wanted to be.</p>
                <a href="/my-account"><h4>Register</h4></a>
            </div>
    </section>

	<!-- HERO SECTION END -->

	<!-- GROWTH SECTION -->
	<section class='growth'>
		<div class="head_text">
			<p class="thin"><?php the_field('about'); ?></p>
		</div>

    </section>
	<!-- GROWTH SECTION END -->

<!-- ICON SECTION -->
<section class="summary_icons">
    <div class="wrapper">
        <div class="s_icon">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/1on1.png">
            <p>One on One</p>
        </div>
        <div class="s_icon">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/expert.png">
            <p>Expert Knowledge</p>
        </div>
        <div class="s_icon">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/online.png">
            <p>Attend Online</p>
        </div>
        <div class="s_icon">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/completion.png">
            <p>Quick Completion</p>
        </div>
    </div>
</section>

<!-- ICON SECTION END -->


<!-- MIDBANNER SECTION -->
<section class="midbanner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/midbanner.jpg)">
    <div class="overlay">
        <div class='text_box'>
            <p>A Team of Professionals</p>
            <h3>Ready to share their knowledge</h3>
        </div>
    </div>
</section>

<!-- MIDBANNER SECTION END -->

	<!-- QUOTE SECTION -->
	<div class="quote_slider">
		<div class='quotes'>
			<img src="<?php the_field('eric_image'); ?>">
            <div class="quote_text">
                <h3>Eric Schleien</h3>
                <p><?php the_field('eric_text'); ?></p>
                <a href='/coaches/eric-schleien/' class='button'> READ MORE</a>
            </div>
		</div>
        <div class='quotes'>
			<img src="<?php the_field('john_image'); ?>">
            <div class="quote_text">
                <h3>John King</h3>
                <p><?php the_field('john_text'); ?></p>
                <a href='/coaches/john-king/' class='button'> READ MORE</a>
            </div>
		</div>
        <div class='quotes'>
			<img src="<?php the_field('nathan_image'); ?>">
            <div class="quote_text">
                <h3>Nathan Fatal</h3>
                <p><?php the_field('nathan_text'); ?></p>
                <a href='/coaches/nathan-fatal/' class='button'> READ MORE</a>
            </div>
		</div>
	</div>
	<!-- QUOTE SECTION END  -->


	<!-- GROWTH SECTION -->
	<section class='growth'>
		<div class="head_text">
			<p>Our Classes</p>
			<h3>Choose A Class to learn more</h3>
		</div>
        <div class="classes">
            <div class="class">
                <div class="class_box">
                    <h4>$200</h4>
                    <p>Online</p>
                    <a href='/product/online-class/' class='button'> LEARN MORE </a>
                </div>
            </div>
            <div class="class">
                <div class="class_box">
                    <h4>$300</h4>
                    <p>In Person</p>
                    <a href='/product/in-person-class/' class='button'> LEARN MORE </a>
                </div>
            </div>
        </div>

    </section>
	<!-- GROWTH SECTION END -->


	<!-- AUDIOBOOK SECTION -->
	<section class='audiobook'>
        <div class="graphic">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/audiobook.jpg">
			<p><?php the_field('step_one'); ?></p>
		</div>
		<div class="copy">
			<h3>Not ready to commit yet?</h3>
            <p>Try out our free audiobook to get a taste of what you will learn, and how you can transform yourself.</p>
            <div class="talk_button">
                <div>
                    <a href="/contact">
                       <button class='lets_talk button'> DOWNLOAD </button>
                    </a>
                </div>
            </div>
            
		</div>



    </section>
	<!-- AUDIOBOOK SECTION ENDS -->

<!-- CTABANNER SECTION -->
<section class="ctabanner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/ctabanner.jpg)">
    <div class="overlay">
        <div class='text_box'>
            <a href='/about' class='button'> HOW IT WORKS</a>
        </div>
    </div>
</section>

<!-- CTABANNER SECTION END -->





  <script type="text/javascript">
    $(document).ready(function(){
      $('.quote_slider').slick({
        autoplay:true,
		autoplaySpeed:2500,
		dots: true,
          arrows: true
      });
    });
  </script>

<?php
get_footer();
