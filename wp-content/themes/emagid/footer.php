<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>

	<!-- FOOTER SECTION STARTS -->
	<footer>
        
		 
		<div class='footer_holder'>
            			<div class="updates newsletter">
				<div class='left_center'>
                    <a href="/"><p class='logo'>TRIBAL COACHING</p></a> 
                    <a href='/my-account' class='button'> SIGN UP NOW</a>

<!--
                    <div class="newsletter_form">
                        <?php echo do_shortcode('[contact-form-7 id="170" title="Newsletter"]'); ?>
                    </div>
-->
                    

<!--
					<div class="social-icons">
						<a href="#" class="facebook"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb.png"></a>
						<a href="#" class="twitter"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter.png"></a>
					</div>
-->
				</div>
			</div>
            
			<div class="links">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-2'
                    ) );
                ?>
			</div>
			
<!--
			<div class="news">
				<p class='industry'>INDUSTRY NEWS</p>
                
                            <//?php
	  			$args = array(
	    		'post_type' => 'industry_news'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
                <a href="<?php the_field('link'); ?>" target="_blank">
                <p><?php the_field('short_intro'); ?></p>
                </a>
                
                
            <//?php
                }
                    }
                else {
                echo 'No Projects Found';
                }
            ?> 
			</div>
-->
            

		</div>
	</footer>
	<!-- FOOTER SECTION ENDS -->



	

<?php wp_footer(); ?>

</body>
</html>
